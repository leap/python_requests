-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: python-requests
Binary: python-requests
Architecture: all
Version: 0.14.0-1
Maintainer: Kristina Clair <kclair@leap.se>
Standards-Version: 3.8.4
Build-Depends: python-setuptools (>= 0.6b3), debhelper (>= 7), python-support (>= 0.8.4)
Checksums-Sha1: 
 62223f0db09f33c21213d079efd1b54cbcd50114 365195 python-requests_0.14.0.orig.tar.gz
 03c2944e473830dee12a2b54b61cdf6e011e6e95 12212 python-requests_0.14.0-1.debian.tar.gz
Checksums-Sha256: 
 ec55b535daa6ff2904895de06dbb8e5613304611c4056e9f6d2b00f4c6730043 365195 python-requests_0.14.0.orig.tar.gz
 b44198ced737a0318c68ab1a365cb7f93750ff7a10d5d0fe969f70f9314fe06b 12212 python-requests_0.14.0-1.debian.tar.gz
Files: 
 05c721d77772c9d33ded6818ba70cca7 365195 python-requests_0.14.0.orig.tar.gz
 95f012d628916e5b45c34810afb00d2b 12212 python-requests_0.14.0-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: GPGTools - http://gpgtools.org

iQIcBAEBAgAGBQJQbI5mAAoJEJ6m/xxX04TjaiQP/1bUz8NFdMwtIEH9WXXfW/PG
ydbYLOam6paEvKN+WW89DQvRoNh3ypH7+HRrM6Nz4jg+ECBBw0MKCGImWAh8bQSY
xqJPkL2IBPyFqbT1hVKEVhixAVQ99WYoCX829vW+NMb4tOjuHxvdiEZg5qdYt9ai
6gJGxsx//Tv+wsBJh5+DMkMiZszhsrAvtrxrK/NXBuO+eG9pBaYaJAQnLs/TvQLr
dMjQGyMBBVFcjtVTX3WYE+XlVwDtqctCES3Y2SH+Eq4qNmrbZh9zaeSC2R/9PmDH
eVSkQKAzN5P/bFKbIy+EdPo+VZizTpNPYZKFFfkRcJua4yRORUPnd+WyG3J0WcuY
Bm8DrubSoA0PgSWGfNMSJNOjhae8qXVG76NAlBzO87F3A+7UfUGEVxmyWcyqR0Zw
lfYqwHoF+KuDa9teX6360o2QzImoFTZXzbvajpX8OYucJMpZpEhv0Kiy6sF2Dg3s
ZbfsGBTQEpCxMCQmRveVlAiTSt8rBRsybBsq0eXpXE9eJwp5qxaEyedV1R/0dSG5
wE/8Pdec+iPWOaqDn9ITtFDFjC1YWqOjqAGPzFK6ekG+V9lANnx+YoGQRgQNBE7l
MXHU9ikfRBoRRagPGwZPGxeiLDRadGmzlLcSoiL9oEMW0qKtFKpDejVQ4TjBnjHt
bSBZGatEVQ12wEWYEeTR
=pA3A
-----END PGP SIGNATURE-----
