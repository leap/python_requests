Document: python-requests
Title: Debian python-requests Manual
Author: <insert document author here>
Abstract: This manual describes what python-requests is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/python-requests/python-requests.sgml.gz

Format: postscript
Files: /usr/share/doc/python-requests/python-requests.ps.gz

Format: text
Files: /usr/share/doc/python-requests/python-requests.text.gz

Format: HTML
Index: /usr/share/doc/python-requests/html/index.html
Files: /usr/share/doc/python-requests/html/*.html
